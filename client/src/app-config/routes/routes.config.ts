export const ROUTES_CONFIG = {
  LOGIN_PAGE: {
    PATH: 'login',
    LINK: '/login'
  },
  REGISTER_PAGE: {
    PATH: 'register',
    LINK: '/register'
  },
  OVERVIEW_PAGE: {
    PATH: 'overview',
    LINK: '/overview'
  },
  ANALYTICS_PAGE: {
    PATH: 'analytics',
    LINK: '/analytics'
  },
  HISTORY_PAGE: {
    PATH: 'history',
    LINK: '/history'
  },
  ORDER_PAGE: {
    PATH: 'order',
    LINK: '/order'
  },
  CATEGORIES_PAGE: {
    PATH: 'categories',
    LINK: '/categories'
  },
  CATEGORIES_NEW_PAGE: {
    PATH: 'categories/new',
    LINK: '/categories/new'
  },
  CATEGORIES_EDIT_PAGE: {
    PATH: 'categories/:id'
  }
};
