import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthLayoutComponent } from "./shared/layouts/auth-layout/auth-layout.component";
import { SiteLayoutComponent } from "./shared/layouts/site-layout/site-layout.component";
import { ROUTES_CONFIG } from "../app-config/routes/routes.config";
import { LoginPageComponent } from "./login-page/login-page.component";
import { RegisterPageComponent } from "./register-page/register-page.component";
import { AuthGuard } from "./shared/guards/auth.guard";
import { OverviewPageComponent } from "./overview-page/overview-page.component";
import { AnalyticsPageComponent } from "./analytics-page/analytics-page.component";
import { HistoryPageComponent } from "./history-page/history-page.component";
import { OrderPageComponent } from "./order-page/order-page.component";
import { CategoriesPageComponent } from "./categories-page/categories-page.component";
import { CategoriesFormComponent } from "./categories-page/categories-form/categories-form.component";


const routes: Routes = [
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
      },
      {
        path: ROUTES_CONFIG.LOGIN_PAGE.PATH,
        component: LoginPageComponent
      },
      {
        path: ROUTES_CONFIG.REGISTER_PAGE.PATH,
        component: RegisterPageComponent
      }
    ]
  },
  {
    path: '',
    component: SiteLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: ROUTES_CONFIG.OVERVIEW_PAGE.PATH,
        component: OverviewPageComponent,
      },
      {
        path: ROUTES_CONFIG.ANALYTICS_PAGE.PATH,
        component: AnalyticsPageComponent,
      },
      {
        path: ROUTES_CONFIG.HISTORY_PAGE.PATH,
        component: HistoryPageComponent,
      },
      {
        path: ROUTES_CONFIG.ORDER_PAGE.PATH,
        component: OrderPageComponent,
      },
      {
        path: ROUTES_CONFIG.CATEGORIES_PAGE.PATH,
        component: CategoriesPageComponent,
      },
      {
        path: ROUTES_CONFIG.CATEGORIES_NEW_PAGE.PATH,
        component: CategoriesFormComponent,
      },
      {
        path: ROUTES_CONFIG.CATEGORIES_EDIT_PAGE.PATH,
        component: CategoriesFormComponent,
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
