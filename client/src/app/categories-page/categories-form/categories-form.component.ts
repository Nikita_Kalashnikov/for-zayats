import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ROUTES_CONFIG } from "../../../app-config/routes/routes.config";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { CategoriesService } from "../../shared/services/categories.service";
import { switchMap } from "rxjs/operators";
import { of } from "rxjs";
import { MaterialService } from "../../shared/guards/material.service";
import { ICategory } from "../../shared/models/category.interface";

@Component({
  selector: 'app-categories-form',
  templateUrl: './categories-form.component.html',
  styleUrls: ['./categories-form.component.scss']
})
export class CategoriesFormComponent implements OnInit {
  public routerLinks = ROUTES_CONFIG;
  public isNew = true;
  public image: File;
  public imagePreview: string | ArrayBuffer = '';
  public form: FormGroup;
  public category: ICategory;

  @ViewChild('fileUpload') inputRef: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required])
    });

    this.route.params
      .pipe(
        switchMap(
          (params: Params) => {
            if (params['id']) {
              this.isNew = false;
              return this.categoriesService.getById(params.id)
            }

            return of(null)
          }
        )
      )
      .subscribe(
        (category: ICategory) => {
          if (category) {
            this.category = category;
            this.form.patchValue({
              name: category.name
            });
            this.imagePreview = category.imageSrc;
            MaterialService.updateTextInputs();
          }
        },
        error => MaterialService.toast(error.error.message)
      );
  }

  public triggerClick() {
    this.inputRef.nativeElement.click();
  }

  public onFileUpload(event) {
    this.image = event.target.files[0];

    const reader = new FileReader();

    reader.onload = () => {
      this.imagePreview = reader.result;
    };

    reader.readAsDataURL(this.image);
  }

  public deleteCategory() {
    const decision = window.confirm(`Вы уверены, что хотите удалить категорию "${this.category.name}"`);

    if (decision)  {
      this.categoriesService.delete(this.category._id).subscribe(
        res => MaterialService.toast(res.message),
        error => MaterialService.toast(error.error.message),
        () => this.router.navigate(['/categories'])
      )
    }
  }

  public onSubmit(): void {
    let obs$;
    this.form.disable();

    if (this.isNew) {
      obs$ = this.categoriesService.create(this.form.value.name, this.image);
    } else {
      obs$ = this.categoriesService.update(this.category._id, this.form.value.name, this.image);
    }

    obs$.subscribe(
      category => {
        this.category = category;
        MaterialService.toast('Изменения сохранены');
        this.router.navigate(['/categories', this.category._id]);
        this.form.enable();
      },
      error => {
        MaterialService.toast(error.error.message);
        this.form.enable();
      }
    )
  }

}
