import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { PositionsService } from "../../../shared/services/positions.service";
import { MaterialInstance, MaterialService } from "../../../shared/guards/material.service";
import { IPosition } from "../../../shared/models/position.interface";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { response } from "express";

@Component({
  selector: 'app-positions-form',
  templateUrl: './positions-form.component.html',
  styleUrls: ['./positions-form.component.scss']
})
export class PositionsFormComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input('categoryId') categoryId: string;
  public positions: IPosition[] = [];
  public loading: boolean = false;
  public modal: MaterialInstance;
  public form: FormGroup;
  public positionId = null;
  @ViewChild('modal') modalRef: ElementRef;

  constructor(
    private positionsService: PositionsService,
    private cdRef: ChangeDetectorRef
  ) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      cost: new FormControl(null, [Validators.required, Validators.min(1)])
    });

    this.loading = true;
    this.positionsService.getByCategoryId(this.categoryId).subscribe(
      positions => {
        this.positions = positions;
        this.loading = false;
      },
      error => MaterialService.toast(error.error.message)
    )
  }

  ngAfterViewInit(): void {
    this.modal = MaterialService.initModal(this.modalRef);
  }

  ngOnDestroy(): void {
    this.modal.destroy();
  }

  public onSelectPosition(position: IPosition) {
    this.positionId = position._id;
    this.form.patchValue({
      name: position.name,
      cost: position.cost
    });

    this.modal.open();
    MaterialService.updateTextInputs();
  }

  public onAddPosition() {
    this.positionId = null;
    this.form.reset();
    this.modal.open();
  }

  public onCancel() {
    this.modal.destroy();
  }

  public onSubmit() {
    this.form.disable();

    const newPosition: IPosition = {
      name: this.form.value.name,
      cost: this.form.value.cost,
      category: this.categoryId
    };

    const completed = () => {
      this.modal.close();
      this.form.reset();
      this.form.enable();
    };

    if (this.positionId) {
      newPosition._id = this.positionId;
      this.positionsService.updatePosition(newPosition).subscribe(
        position => {
          const idx = this.positions.findIndex(p => p._id === position._id);
          this.positions[idx] = position;
          MaterialService.toast('Изменения сохранены');
        },
        error => MaterialService.toast(error.error.message),
        completed
      )
    } else {
      this.positionsService.createPosition(newPosition).subscribe(
        position => {
          MaterialService.toast('Позиция создана');
          this.positions.push(position);
        },
        error => MaterialService.toast(error.error.message),
        completed
      )
    }
  }

  public onDeletePosition(e: Event, position: IPosition) {
    e.stopPropagation();
    const decision = window.confirm(`Удалить позицию '${position.name}'`);

    if (decision) {
      this.positionsService.deletePosition(position).subscribe(
        (res) => {
          const idx = this.positions.findIndex(p => p._id === position._id);
          this.positions.splice(idx, 1);
          MaterialService.toast(res.message);
        },
        error => MaterialService.toast(error.message.message)
      )
    }
  }

}
