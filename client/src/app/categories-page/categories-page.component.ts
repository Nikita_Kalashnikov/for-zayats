import { Component, OnInit } from '@angular/core';
import { ROUTES_CONFIG } from "../../app-config/routes/routes.config";
import { CategoriesService } from "../shared/services/categories.service";
import { ICategory } from "../shared/models/category.interface";
import { Observable } from "rxjs";

@Component({
  selector: 'app-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.scss']
})
export class CategoriesPageComponent implements OnInit {
  public routerLinks = ROUTES_CONFIG;
  public categories$: Observable<ICategory[]>;

  constructor(
    private categoriesService: CategoriesService
  ) { }

  ngOnInit(): void {
    this.categories$ = this.categoriesService.fetch()
  }

}
