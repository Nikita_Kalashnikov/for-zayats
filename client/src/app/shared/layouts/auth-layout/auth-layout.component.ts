import { Component, OnInit } from '@angular/core';
import { ROUTES_CONFIG } from "../../../../app-config/routes/routes.config";

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.scss']
})
export class AuthLayoutComponent implements OnInit {
  public ROUTES_CONFIG = ROUTES_CONFIG;

  constructor() { }

  ngOnInit(): void {
  }

}
