import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ROUTES_CONFIG } from "../../../../app-config/routes/routes.config";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { MaterialService } from "../../guards/material.service";

@Component({
  selector: 'app-site-layout',
  templateUrl: './site-layout.component.html',
  styleUrls: ['./site-layout.component.scss']
})
export class SiteLayoutComponent implements AfterViewInit {
  public routerLinks = ROUTES_CONFIG

  @ViewChild('fixedActionBtn') floatingRef: ElementRef;

  public links = [
    {
      url: ROUTES_CONFIG.OVERVIEW_PAGE.LINK,
      name: 'Обзор'
    },
    {
      url: ROUTES_CONFIG.ANALYTICS_PAGE.LINK,
      name: 'Аналитика'
    },
    {
      url: ROUTES_CONFIG.HISTORY_PAGE.LINK,
      name: 'История'
    },
    {
      url: ROUTES_CONFIG.ORDER_PAGE.LINK,
      name: 'Добавление заказа'
    },
    {
      url: ROUTES_CONFIG.CATEGORIES_PAGE.LINK,
      name: 'Ассортимент'
    },
  ];
  constructor(
    private auth: AuthService,
    private router: Router
  ) {
  }

  ngAfterViewInit(): void {
    MaterialService.initializeFloatingButton(this.floatingRef)
  }

  public logout(): void {
    this.auth.logout();
    this.router.navigate([ROUTES_CONFIG.LOGIN_PAGE.PATH])
  }

}
