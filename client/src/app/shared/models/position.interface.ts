export interface IPosition {
  name: string,
  cost: number,
  category: string,
  user?: string,
  _id?: string
}
