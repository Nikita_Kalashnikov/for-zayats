import { Injectable } from '@angular/core';
import { IUser } from "../models/user.interface";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private token: string | null = null;

  constructor(private http: HttpClient) {
  }

  public register(user: IUser): Observable<IUser> {
    return this.http.post<IUser>('/api/auth/register', user)
  }

  public login(user: IUser): Observable<{ token: string }> {
    return this.http.post<{ token: string }>('/api/auth/login', user)
      .pipe(
        tap(
          ({token}) => {
            localStorage.setItem('auth-token', token);
            this.setToken(token)
          }
        )
      )
  }

  public setToken(token: string | null): void {
    this.token = token
  }

  public getToken(): string {
    return this.token
  }

  public isAuthenticated(): boolean {
    return !!this.token
  }

  public logout(): void {
    this.setToken(null);
    localStorage.clear();
  }
}
