import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { IPosition } from "../models/position.interface";
import { Message } from "@angular/compiler/src/i18n/i18n_ast";

@Injectable({
  providedIn: 'root'
})
export class PositionsService {

  constructor(private http: HttpClient) { }

  public getByCategoryId(id: string): Observable<IPosition[]> {
    return this.http.get<IPosition[]>(`/api/position/${id}`)
  }

  public createPosition(position: IPosition): Observable<IPosition> {
    return this.http.post<IPosition>('/api/position', position);
  }

  public updatePosition(position: IPosition): Observable<IPosition> {
    return this.http.patch<IPosition>(`/api/position/${position._id}`, position);
  }

  public deletePosition(position: IPosition):Observable<any> {
    return this.http.delete<any>(`/api/position/${position._id}`)
  }
}
